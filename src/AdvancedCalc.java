import java.util.Scanner;

class AdvancedCalc extends SimpleCalc {
    public AdvancedCalc() {
        super (Num_1, Num_2, Add, Div, Mul, Sub);
    }

    Scanner input = new Scanner(System.in);

    public double getSquare(double sqr){
        System.out.println("Введите основание треугольника: ");
        double h = input.nextDouble();
        System.out.println("Введите высоту треугольника: ");
        double a = input.nextDouble();
        return (a/2) * h;
    }

    public double getSquare1(double sqr) {
        System.out.println("Введите длинну большой полуоси: ");
        double с = input.nextDouble();
        System.out.println("Введите длинну малой полуоси: ");
        double b = input.nextDouble();
        return (Math.PI * с) * b;
    }

}